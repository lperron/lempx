# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/05/09 12:30:59 by lperron      #+#   ##    ##    #+#        #
#    Updated: 2019/08/04 10:38:04 by lperron     ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

CC= gcc

all: fdf

libs:
	make -C ./libft/
	cp ./libft/libft.a ./fdf/libft.a
ifeq ($(shell uname -s),Linux)
	make -C ./minilibx/
	cp ./minilibx/libmlx_Linux.a ./fdf/libmlx.a
endif
ifeq ($(shell uname -s),Darwin)
	make -C ./minilibx_macos/
	cp ./minilibx_macos/libmlx.a ./fdf/libmlx.a
endif
	make -C ./lempx/
	cp ./lempx/liblempx.a ./fdf/liblempx.a


fdf: libs
	make -C fdf

clean: 
	make -C ./libft/ clean
	make -C ./minilibx/ clean
	make -C ./minilibx_macos/ clean
	make -C ./lempx/ clean
	make -C ./fdf/ clean

fclean:
	make -C ./libft/ fclean
	make -C ./minilibx/ clean
	make -C ./minilibx_macos/ clean
	make -C ./lempx/ fclean
	make -C ./fdf/ fclean
	rm -f ./fdf/libft.a 
	rm -f ./fdf/libmlx.a 
	rm -f ./fdf/liblempx.a

re: fclean all
