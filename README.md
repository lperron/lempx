# LempX     :heart:

<p align='center'>LempX (Lucas Etienne Maitre Perron X) is a graphical library strongly inspired by OpenGL and which uses only the minilibX in order to fit to 42's projects.<\p>
<p align='center'>The lib is not finished yet, but all geometrical transformations work well, lines rendering and clipping too.<\p>
<p align='center'><img src='https://gitlab.com/lperron/lempX/raw/master/exp/logolempx.png'</p>

## Performances

<p align='center'>All LempX's transformations are achieved using 4X4 matrices which are multplied between them and finaly by each vertex, so we keep the minimal amount of calculation for each vertex<\p>
<p align='center'>For example, if we want to apply a translation, then a rotation, another translation and finaly a projection to 1M vertices, lempX will combine the four transformations then multiplying each vertex by the resulting matrix. We will therefore only need to compute 3 Matrix-Matrix multiplications and 1M Matrix-Vector multiplications (which is far better than applying each transformations to all vertices). <\p>
<p align='center'>Furthermore, LempX is natively threaded and uses minilibX images!<\p>

# Demo

## FDF :

<p align='center'>FDF is the first graphical project of 42 school. The goal is to render a wireframe model of a heights map.<\p>
<p align='center'>Using LempX, I only spent four days to finish this project, with a FPS cam, an arcball one, perspective and ortho projections, and many other things. In addition my FDF can easily render 1M vertices!<\p>
<p align='center'>I also implemented a procedural map generator based on a tweaked diamond square algorithm.<\p>

# Screenshots

## FDF auto-map :



 <figure>
  <img src="https://gitlab.com/lperron/lempX/raw/master/exp/fdf_capture.png" alt="EX1" style="width:100%">
  <figcaption>Procedurally generated map, created and rendered by fdf using lempX lib.</figcaption>
</figure> 

 <figure>
  <img src="https://gitlab.com/lperron/lempX/raw/master/exp/fdf_ex2.png" alt="EX2" style="width:100%">
  <figcaption>Procedurally generated map, created and rendered by fdf using lempX lib.</figcaption>
</figure> 

 <figure>
  <img src="https://gitlab.com/lperron/lempX/raw/master/exp/messingwithfov.png" alt="Messing with FOV" style="width:100%">
  <figcaption>Messing with FOV, Fract'ol is coming....</figcaption>
</figure> 

# Video

<figure>
  <img src="https://gitlab.com/lperron/files_for_other_projects/raw/master/FDFDemoGit.mp4" alt="Free Camera" style="width:100%">
</figure> 

