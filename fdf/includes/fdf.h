/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 15:35:53 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 14:45:57 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "lempx.h"
# include <stdio.h>
# include <math.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# ifdef __APPLE__
#  define SIZE_DIS 1040
#  define TEXT_OFF_Y 8
#  define TEXT_OFF_X 27
#  define MADE_OFF_X 1779
#  define LOVE_OFF_Y 9
#  define LOVE_OFF_X 1875
# else
#  define SIZE_DIS 995
#  define TEXT_OFF_Y 17
#  define TEXT_OFF_X 27
#  define MADE_OFF_X 1810
#  define LOVE_OFF_Y 3
#  define LOVE_OFF_X 1875
# endif

# ifdef __APPLE__
#  define Z_CHAR 13
#  define S_CHAR 1
#  define Q_CHAR 0
#  define D_CHAR 2
#  define O_CHAR 31
#  define I_CHAR 34
#  define P_CHAR 35
#  define C_CHAR 8
#  define M_CHAR 46
#  define R_CHAR 69
#  define F_CHAR 78
#  define ESC_CHAR 53
# else
#  define Z_CHAR 'z'
#  define S_CHAR 's'
#  define Q_CHAR 'q'
#  define D_CHAR 'd'
#  define O_CHAR 'o'
#  define I_CHAR 'i'
#  define P_CHAR 'p'
#  define C_CHAR 'c'
#  define M_CHAR 'm'
#  define R_CHAR 'r'
#  define F_CHAR 'f'
#  define ESC_CHAR 65307
# endif

# define COL_Z 0
# define COL_M 1

struct							s_hook_struct
{
	t_lempx_ptr					*tt;
	t_camera					*cam;
	t_model						**scene;
	int							scene_size;
	char						*key;
	t_point						mouse;
	t_cam_mode					cam_mode;
	void						*other;
	char						color_mode;
};

typedef	struct s_hook_struct	t_hook;

/*
** fdf_colorize.c
*/
void							interp_z_col(t_vertex *v, double mmm[3],
								int type);
void							set_color_from_z(t_model *mod);
void							set_color_map(t_model *mod);
/*
** fdf_render.c
*/
void							render_scene(t_hook *data);
void							put_info(t_hook *data);
void							put_love(t_hook *data);
/*
** fdf_hook.c
*/
int								destroy_hook(t_hook *data);
int								loop_hook(t_hook *data);
int								handle_key_pressed(int key, t_hook *data);
int								handle_key_released(int key, t_hook *data);
int								mouse_hook(int x, int y, t_hook *data);
/*
** fdf_input.c
*/
t_model							*get_map(char *file, int *size);
int								*read_map(int fd, int *nb_point, int *x, int s);
int								ft_nb_point(char *line);
int								fill_line(int *map, char *line,
								int *xi, int *nb_p);
int								add_point_to_map(int *map, int z, int *nb_p);
/*
** fdf_model.c
*/
int								scale_scene(t_hook *data);
void							fill_model(t_model **mod, int *map,
								int nb_point, int *max);
t_model							*choose_map(int argc,
								char **argv, int *map_size);
/*
** fdf_cam.c
*/
void							change_cam_mode(t_hook *data);
void							change_projection_mode(t_hook *data);
/*
** fdf_auto.c
*/
t_model							*generate_map(int x,
								float smooth, int *map_size);
/*
** fdf_square_diamond.c
*/
void							square_diamond(double **map, int max,
								int size, float scale);
/*
** lempx_fps.c
*/
int								fdf_fps_hook(t_hook *data);
int								fdf_fps_fov(t_hook *data);
/*
**lempx_arcball.c
*/
int								fdf_arcball_hook(t_hook *data);
#endif
