/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_arcball.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/01 16:30:50 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:08:29 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <math.h>
#include "fdf.h"

static	int	arc_rotation(int dx, int dy, t_vector eye)
{
	double	yaw;
	double	pitch;
	double	r;
	int		sign;
	double	dir[3];

	if (dx == 0 && dy == 0)
		return (0);
	r = norm_vector(eye, 3);
	sign = eye[2] > 0 ? 0 : 1;
	yaw = atan(-eye[0] / eye[2]) + dx / 100. + M_PI * sign;
	pitch = asin(-eye[1] / r) + dy / 100.;
	pitch = pitch > M_PI / 2. ? M_PI / 2. : pitch;
	pitch = pitch < -M_PI / 2. ? -M_PI / 2. : pitch;
	lempx_set_vector3(dir, -cos(pitch) * sin(yaw),
			sin(pitch), cos(pitch) * cos(yaw));
	lambda_vector_multiplication(r / norm_vector(dir, 3), dir, 3);
	lempx_set_vector3(eye, dir[0], -dir[1], dir[2]);
	return (1);
}

static	int	fdf_arcball_key(t_hook *data, int *dx, int *dy)
{
	int	change;

	change = 0;
	if (data->key[I_CHAR] == 1 && (++change))
		lambda_vector_multiplication(0.995, data->cam->eye, 3);
	if (data->key[O_CHAR] == 1 && (++change))
		lambda_vector_multiplication(1.005, data->cam->eye, 3);
	if (data->key[Z_CHAR] == 1 && (++change))
		(*dy)--;
	if (data->key[S_CHAR] == 1 && (++change))
		(*dy)++;
	if (data->key[D_CHAR] == 1 && (++change))
		(*dx)--;
	if (data->key[Q_CHAR] == 1 && (++change))
		(*dx)++;
	return (change);
}

int			fdf_arcball_hook(t_hook *data)
{
	t_point	d;
	int		change;

	d = init_point(0, 0);
	change = fdf_arcball_key(data, &(d.x), &(d.y));
	if (!change)
		return (0);
	if (d.x || d.y)
		arc_rotation(d.x, d.y, data->cam->eye);
	lempx_look_at(data->cam, data->cam->eye,
			data->cam->center, data->cam->up);
	lempx_update_camera(data->cam);
	return (1);
}
