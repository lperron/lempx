/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_auto.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/05 02:37:56 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:59:38 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

double	filtering(double **map, int x, int y, int max)
{
	int		nb;
	double	sum;
	int		depth;
	int		i;
	int		j;

	nb = 0;
	sum = 0;
	depth = max / 50;
	depth = depth < 2 ? 2 : depth;
	i = -depth;
	while (++i < depth)
	{
		j = -depth;
		while (++j < depth)
		{
			if (y + j >= 0 && y + j < max && x + i >= 0 && x + i < max && nb++)
				sum += map[x + i][y + j];
		}
	}
	return (sum / (double)nb);
}

void	low_pass(double **map, t_model *mod, int *map_size, int max)
{
	int		i;
	int		j;
	double	tmp;

	j = -1;
	while (++j < max && (i = -1))
		while (++i < max)
		{
			tmp = filtering(map, i, j, max);
			lempx_model_add_vertex(mod, (double)i, (double)j, tmp);
			map_size[2] = fmax(tmp, map_size[2]);
		}
}

t_model	*auto_model(double **map, int max, int *map_size)
{
	t_model	*mod;
	int		i;

	if ((mod = malloc(sizeof(t_model))))
	{
		*mod = lempx_init_model(LEMPX_WIREFRAME);
		lempx_model_new_primitive(mod, max * max);
		if (mod->primitive)
			low_pass(map, mod, map_size, max);
	}
	i = -1;
	while (++i < max)
		free(map[i]);
	free(map);
	if (mod->primitive == NULL)
	{
		free(mod);
		return (NULL);
	}
	return (mod);
}

void	init_map(double **map, double scale, int max, int min)
{
	int	j;
	int	k;

	j = -1;
	while (++j < max && (k = -1))
		while (++k < max)
			map[k][j] = (rand() / (double)RAND_MAX * scale * 2 - scale)
				/ 150.;
	map[min][min] = rand() / (double)RAND_MAX * scale * 2 - scale;
	map[max - 1][max - 1] = rand() / (double)RAND_MAX * scale * 2 - scale;
	map[min][max - 1] = rand() / (double)RAND_MAX * scale * 2 - scale;
	map[max - 1][min] = rand() / (double)RAND_MAX * scale * 2 - scale;
}

t_model	*generate_map(int x, float smooth, int *map_size)
{
	double	**map;
	int		min;
	int		max;
	int		i;
	float	scale;

	srand(time(NULL));
	if (!(map = malloc(sizeof(double*) * x * 2)))
		return (NULL);
	i = -1;
	while (++i < x)
		if (!(map[i] = ft_memalloc(sizeof(double) * x * 2)))
			return (NULL);
	min = 0;
	max = x;
	scale = smooth * x;
	map_size[0] = max;
	map_size[1] = max;
	map_size[2] = INT_MIN;
	init_map(map, scale, max, min);
	square_diamond(map, max, max, smooth);
	return (auto_model(map, max, map_size));
}
