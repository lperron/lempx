/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_cam.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 16:39:40 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/04 16:43:03 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	change_cam_mode(t_hook *data)
{
	(data->cam_mode)++;
	if (data->cam_mode == 3)
		data->cam_mode = 0;
	if (data->cam_mode == 2)
		set_vector(data->cam->center, 3, 0., 0., 0.);
	lempx_look_at(data->cam, data->cam->eye,
			data->cam->center, data->cam->up);
	lempx_update_camera(data->cam);
	put_info(data);
	render_scene(data);
}

void	change_projection_mode(t_hook *data)
{
	if (data->cam->proj_type == LEMPX_ORTHO)
	{
		lempx_set_projection(data->cam, LEMPX_PERSPECTIVE);
		lempx_change_fov(data->cam, 50.);
		data->key[I_CHAR] = 0;
		data->key[O_CHAR] = 0;
	}
	else
		lempx_set_projection(data->cam, LEMPX_ORTHO);
	lempx_update_camera(data->cam);
	put_info(data);
	render_scene(data);
}
