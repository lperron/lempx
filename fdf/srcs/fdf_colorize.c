/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_colorize.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 15:34:48 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/04 15:43:37 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	interp_z_col(t_vertex *v, double mmm[3], int type)
{
	double	co;

	v->color.color = 0;
	if (type == 0)
	{
		co = mmm[1] != mmm[2] ? (v->point[2] - mmm[1]) / (mmm[2] - mmm[1]) : 1;
		v->color.rgb[1] = (unsigned char)(255 * (1 - co));
		v->color.rgb[2] = (unsigned char)(255 * co);
	}
	else
	{
		co = mmm[0] != mmm[1] ? (v->point[2] - mmm[0]) / (mmm[1] - mmm[0]) : 1;
		v->color.rgb[0] = (unsigned char)(255 * (1 - co));
		v->color.rgb[1] = (unsigned char)(255 * co);
	}
	v->color.rgb[3] = (unsigned char)0;
}

void	set_color_from_z(t_model *mod)
{
	double	mmm[3];
	int		i;

	mmm[2] = -HUGE_VAL;
	mmm[0] = HUGE_VAL;
	i = -1;
	while (++i < mod->primitive[0].nb_vertex)
	{
		if (mod->primitive[0].vertex[i].point[2] > mmm[2])
			mmm[2] = mod->primitive[0].vertex[i].point[2];
		if (mod->primitive[0].vertex[i].point[2] < mmm[0])
			mmm[0] = mod->primitive[0].vertex[i].point[2];
	}
	mmm[1] = (mmm[0] + mmm[2]) / 2.;
	i = -1;
	while (++i < mod->primitive[0].nb_vertex)
	{
		if (mod->primitive[0].vertex[i].point[2] >= mmm[1])
			interp_z_col(&(mod->primitive[0].vertex[i]), mmm, 0);
		if (mod->primitive[0].vertex[i].point[2] < mmm[1])
			interp_z_col(&(mod->primitive[0].vertex[i]), mmm, 1);
	}
}

void	set_color_map(t_model *mod)
{
	int	i;

	i = -1;
	while (++i < mod->primitive[0].nb_vertex)
	{
		if (mod->primitive[0].vertex[i].point[2] < -10)
			mod->primitive[0].vertex[i].color.color = lempx_rgb(0, 0, 70);
		else if (mod->primitive[0].vertex[i].point[2] < 1)
			mod->primitive[0].vertex[i].color.color = lempx_rgb(0, 112, 151);
		else if (mod->primitive[0].vertex[i].point[2] < 40)
			mod->primitive[0].vertex[i].color.color = lempx_rgb(79, 140, 79);
		else if (mod->primitive[0].vertex[i].point[2] < 170)
			mod->primitive[0].vertex[i].color.color = lempx_rgb(140, 100, 40);
		else
			mod->primitive[0].vertex[i].color.color = lempx_rgb(216, 216, 216);
	}
}
