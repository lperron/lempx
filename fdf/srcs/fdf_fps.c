/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_fps.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/01 14:56:16 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:09:36 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <math.h>
#include "fdf.h"

static	int	fdf_fps_keyboard(t_hook *data, t_vector eye, t_vector center)
{
	double		move[3];
	double		cross[3];
	t_point		s;

	s = init_point(0, 0);
	if (data->key[Z_CHAR] == 1)
		s.x--;
	if (data->key[S_CHAR] == 1)
		s.x++;
	if (data->key[D_CHAR] == 1)
		s.y++;
	if (data->key[Q_CHAR] == 1)
		s.y--;
	if (!(s.x || s.y))
		return (0);
	lempx_set_vector3(move, eye[0] - center[0], eye[1] - center[1], eye[2]
		- center[2]);
	lambda_vector_multiplication(0.005, move, 3);
	lempx_cross_product3(cross, move, data->cam->up);
	lambda_vector_multiplication(s.y, cross, 3);
	lempx_set_vector3(eye, eye[0] + cross[0] + move[0] * s.x, eye[1] +
	cross[1] + move[1] * s.x, eye[2] + cross[2] + move[2] * s.x);
	lempx_set_vector3(center, center[0] + cross[0] + move[0] * s.x,
center[1] + cross[1] + move[1] * s.x, center[2] + cross[2] + move[2] * s.x);
	return (1);
}

static	int	fdf_fps_follow_mouse(t_hook *d, t_vector e, t_vector c)
{
	double		pitch;
	double		yaw;
	double		r;
	double		dir[3];
	int			sign;

	yaw = -(d->mouse.x - d->tt->size.x / 2);
	pitch = -(d->mouse.y - d->tt->size.y / 2);
	yaw = ft_abs((int)yaw) > 0.05 * d->tt->size.x ? yaw / 50000. : 0;
	pitch = ft_abs((int)pitch) > 0.05 * d->tt->size.y ? pitch /
		50000. : 0;
	if (yaw == 0 && pitch == 0)
		return (0);
	lempx_set_vector3(dir, c[0] - e[0], c[1] - e[1], -c[2] + e[2]);
	lambda_vector_multiplication(1 / norm_vector(dir, 3), dir, 3);
	sign = dir[2] < 0 ? 1 : 0;
	yaw += atan(dir[0] / dir[2]) + M_PI * sign;
	pitch += asin(dir[1]);
	if ((pitch >= M_PI_2 || pitch <= -M_PI_2))
		return (1);
	r = sqrt(pow(e[2] - c[2], 2) + pow(e[1] - c[1], 2)
		+ pow(e[0] - c[0], 2));
	lempx_set_vector3(c, e[0] + r * cos(pitch) * sin(yaw), e[1] +
		r * sin(pitch), (e[2] - r * cos(pitch) * cos(yaw)));
	return (1);
}

int			fdf_fps_fov(t_hook *data)
{
	double fovy;
	double prev_fovy;

	fovy = data->cam->fovy * 180 / (double)M_PI;
	prev_fovy = fovy;
	if (data->key[I_CHAR] == 1)
		fovy--;
	if (data->key[O_CHAR] == 1)
		fovy++;
	if (fovy >= 179 || fovy <= 1)
		fovy = prev_fovy;
	if (prev_fovy == fovy)
		return (0);
	lempx_change_fov(data->cam, fovy);
	return (1);
}

int			fdf_fps_hook(t_hook *data)
{
	int	change;

	change = 0;
	change += fdf_fps_keyboard(data, data->cam->eye,
					data->cam->center);
	change += fdf_fps_fov(data);
	change += fdf_fps_follow_mouse(data, data->cam->eye,
					data->cam->center);
	if (!change)
		return (0);
	lempx_look_at(data->cam, data->cam->eye,
		data->cam->center, data->cam->up);
	lempx_update_camera(data->cam);
	return (1);
}
