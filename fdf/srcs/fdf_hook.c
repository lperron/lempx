/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_hook.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 15:57:43 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/04 18:26:11 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		destroy_hook(t_hook *data)
{
	int	i;

	i = -1;
	while (++i < data->scene_size)
		lempx_free_model(data->scene[i]);
	mlx_destroy_image(data->tt->mlx_ptr, data->other);
	lempx_free_camera(data->cam);
	lempx_destroy(data->tt);
	exit(0);
	return (0);
}

int		loop_hook(t_hook *data)
{
	int		change;
	double	prev_fov;

	prev_fov = data->cam->fovy;
	usleep(2000);
	change = 0;
	if (data->cam_mode == LEMPX_FPS)
		change = fdf_fps_hook(data);
	else if (data->cam_mode == LEMPX_ARCBALL)
		change = fdf_arcball_hook(data);
	change += scale_scene(data);
	if (!change)
		return (0);
	if (data->cam->fovy != prev_fov)
		put_info(data);
	render_scene(data);
	return (0);
}

int		handle_key_pressed(int key, t_hook *data)
{
	if (key == Z_CHAR && data->cam->proj_type == LEMPX_ORTHO &&
			data->cam_mode == LEMPX_FPS)
		data->key[I_CHAR] = 1;
	else if (key == S_CHAR && data->cam->proj_type == LEMPX_ORTHO &&
			data->cam_mode == LEMPX_FPS)
		data->key[O_CHAR] = 1;
	else if (key >= 0 && key < 500 && key != ESC_CHAR)
		data->key[key] = 1;
	else if (key == ESC_CHAR)
		destroy_hook(data);
	if (key == C_CHAR)
		change_cam_mode(data);
	if (key == P_CHAR)
		change_projection_mode(data);
	if (key == M_CHAR)
	{
		data->color_mode = !(data->color_mode);
		if (data->color_mode == COL_Z)
			set_color_from_z(data->scene[0]);
		else
			set_color_map(data->scene[0]);
		render_scene(data);
	}
	return (0);
}

int		handle_key_released(int key, t_hook *data)
{
	if (key == Z_CHAR && data->cam->proj_type == LEMPX_ORTHO &&
			data->cam_mode == LEMPX_FPS)
		data->key[I_CHAR] = 0;
	else if (key == S_CHAR && data->cam->proj_type == LEMPX_ORTHO &&
			data->cam_mode == LEMPX_FPS)
		data->key[O_CHAR] = 0;
	if (key >= 0 && key < 500)
		data->key[key] = 0;
	return (0);
}

int		mouse_hook(int x, int y, t_hook *data)
{
	data->mouse.x = x;
	data->mouse.y = y;
	return (0);
}
