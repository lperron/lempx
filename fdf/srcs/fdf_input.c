/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_input.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 16:02:43 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 14:50:06 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		add_point_to_map(int *map, int z, int *nb_p)
{
	(map)[*nb_p] = z;
	(*nb_p)++;
	return (1);
}

int		fill_line(int *map, char *l, int *xi, int *nb_p)
{
	int		i;
	int		x;

	i = 0;
	x = 0;
	while (l[i])
	{
		while (l[i] && ft_ischarset(l[i], " \t\n\r\v\f"))
			i++;
		if ((l[i] != '\0') && ((!((l[i] >= '0' && l[i] <= '9') || l[i] == '+' ||
				l[i] == '-')) || !add_point_to_map(map, ft_atoi(&l[i]), nb_p)))
		{
			x = -1;
			break ;
		}
		if (l[i])
			x++;
		while (l[i] && (((l[i] >= '0' && l[i] <= '9')
						|| l[i] == '+' || l[i] == '-')))
			i++;
	}
	if (((*xi != -1 && *xi != x) || x < 0))
		return (0);
	*xi = x;
	return (1);
}

int		ft_nb_point(char *line)
{
	int	i;
	int	x;

	i = 0;
	x = 0;
	while (line[i])
	{
		while (line[i] && ft_ischarset(line[i], " \t\n\r\v\f"))
			i++;
		if ((line[i] != '\0') && (!((line[i] >= '0' && line[i] <= '9') ||
						line[i] == '+' || line[i] == '-')))
		{
			x = -1;
			break ;
		}
		if (line[i])
			x++;
		while (line[i] && ((line[i] >= '0' && line[i] <= '9') || line[i] == '+'
					|| line[i] == '-'))
			i++;
	}
	return (x);
}

int		*read_map(int fd, int *nb_point, int *x, int s)
{
	int		*map;
	char	*line;
	t_point	coord;

	if (!(map = malloc(sizeof(map) * 1024)))
		return (NULL);
	coord = init_point(-1, 0);
	while (get_next_line(fd, &line))
	{
		while (ft_nb_point(line) > s - *nb_point && (s *= 2))
			if (!(ft_realloc((void**)&map, s / 2 * sizeof(int),
							(s * sizeof(int)))))
				return (0);
		if (!fill_line(map, line, &coord.x, nb_point))
		{
			close(fd);
			ft_super_free(2, line, map);
			return (NULL);
		}
		free(line);
		coord.y++;
	}
	*x = coord.x;
	return (map);
}

t_model	*get_map(char *file, int *size)
{
	int		fd;
	t_model	*ret;
	int		nb_point;
	int		*map;

	nb_point = 0;
	if ((fd = open(file, O_RDONLY)) < 0 || read(fd, NULL, 0) < 0)
		return (NULL);
	if (!(ret = malloc(sizeof(t_model))))
	{
		close(fd);
		return (NULL);
	}
	*ret = lempx_init_model(LEMPX_WIREFRAME);
	if (!(map = read_map(fd, &nb_point, &(size[0]), 1024)))
	{
		close(fd);
		free(ret);
		return (NULL);
	}
	close(fd);
	fill_model(&ret, map, nb_point, size);
	free(map);
	return (ret);
}
