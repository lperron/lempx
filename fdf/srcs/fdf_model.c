/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_model.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 16:36:02 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 17:34:11 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		scale_scene(t_hook *data)
{
	if (data->key[R_CHAR] && data->key[F_CHAR])
		return (0);
	if (data->key[R_CHAR])
	{
		lempx_model_scale(data->scene[0], 1., 1., 1.05);
		return (1);
	}
	if (data->key[F_CHAR])
	{
		lempx_model_scale(data->scene[0], 1., 1., 0.95);
		return (1);
	}
	return (0);
}

void	fill_model(t_model **mod, int *map, int nb_point, int *max)
{
	int		x;
	int		y;
	int		i;

	i = -1;
	x = -1;
	y = 0;
	lempx_model_new_primitive(*mod, nb_point);
	if ((*mod)->primitive == NULL)
	{
		free(*mod);
		*mod = NULL;
		return ;
	}
	while (++i < nb_point)
	{
		x++;
		lempx_model_add_vertex(*mod, (double)x, (double)y, (double)map[i]);
		max[2] = map[i] > max[2] ? map[i] : max[2];
		if (x + 2 > max[0] && (x = -1))
			max[1] = y++;
	}
}

t_model	*choose_map(int argc, char **argv, int *map_size)
{
	t_model	*map;
	int		s[2];

	s[0] = 0;
	s[1] = 0;
	if (argc == 2)
	{
		if ((!(map = get_map(argv[1], map_size))) && ft_printf("Map error\n"))
			return (NULL);
		return (map);
	}
	else if (ft_strcmp(argv[1], "-a") == 0 || ft_strcmp(argv[1], "--auto") == 0)
	{
		if (argc != 4 &&
			ft_printf("usage : ./fdf map\n        ./fdf -a SIZE SMOOTHNESS\n"))
			return (0);
		if (((s[0] = ft_atoi(argv[2])) <= 0 || s[0] > 11 ||
					(s[1] = ft_atoi(argv[3])) < 0) && ft_printf("Map error\n"))
			return (0);
		return (generate_map(pow(2, s[0]) + 1, s[1] / 100., map_size));
	}
	ft_putendl("usage : ./fdf map\n        ./fdf -a SIZE SMOOTHNESS");
	return (NULL);
}
