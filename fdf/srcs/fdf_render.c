/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_render.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 15:44:25 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 15:28:48 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	put_love(t_hook *data)
{
	if (!data->other)
		return ;
	mlx_string_put(data->tt->mlx_ptr, data->tt->win_ptr, MADE_OFF_X, SIZE_DIS
			+ TEXT_OFF_Y, lempx_rgb(255, 255, 255), "Made with ");
	mlx_put_image_to_window(data->tt->mlx_ptr, data->tt->win_ptr, data->other,
			LOVE_OFF_X, SIZE_DIS + LOVE_OFF_Y);
}

void	draw_line(t_hook *data)
{
	int	i;

	i = 19;
	while (++i < 1900)
		mlx_pixel_put(data->tt->mlx_ptr, data->tt->win_ptr, i,
				SIZE_DIS + 1, lempx_rgb(255, 255, 255));
	put_love(data);
}

void	put_info(t_hook *data)
{
	char	*str;
	char	*proj;
	char	*cam;
	char	*fov;

	mlx_clear_window(data->tt->mlx_ptr, data->tt->win_ptr);
	proj = data->cam->proj_type == LEMPX_ORTHO ?
ft_strdup("ortho               ") : ft_strdup("perspective         ");
	if (data->cam_mode == LEMPX_FPS)
		cam = ft_strdup("free               ");
	else if (data->cam_mode == LEMPX_ARCBALL)
		cam = ft_strdup("arcball            ");
	else
		cam = ft_strdup("static             ");
	fov = ft_itoa((int)((data->cam->fovy) * 180 / (double)M_PI));
	if (!proj || !cam || !fov)
		return ;
	str = ft_strjoin_mult(6, "Projection : ", proj, "Camera mode : ", cam,
			"FOV = ", fov);
	if (str)
		mlx_string_put(data->tt->mlx_ptr, data->tt->win_ptr,
			TEXT_OFF_X, SIZE_DIS + TEXT_OFF_Y, lempx_rgb(255, 255, 255), str);
	ft_super_free(4, str, proj, cam, fov);
	draw_line(data);
}

void	render_scene(t_hook *data)
{
	int	i;

	i = -1;
	while (++i < data->tt->nb_pixel)
	{
		data->tt->depth[i] = HUGE_VAL;
		data->tt->img_data[i] = 0;
	}
	i = -1;
	while (++i < data->scene_size)
		lempx_render_model(data->scene[0], data->cam, data->tt);
	mlx_put_image_to_window(data->tt->mlx_ptr,
			data->tt->win_ptr, data->tt->image, 0, 0);
}
