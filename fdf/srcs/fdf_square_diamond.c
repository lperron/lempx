/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf_square_diamond.c                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/08 14:00:29 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 14:10:06 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	diamond(double **map, t_point p, t_point halfmax, float scale)
{
	float	sum;
	int		nb;

	sum = 0;
	nb = 0;
	if (p.y - halfmax.x >= 0 && nb++)
		sum += map[p.x][p.y - halfmax.x];
	if (p.y + halfmax.x < halfmax.y && nb++)
		sum += map[p.x][p.y + halfmax.x];
	if (p.x - halfmax.x >= 0 && nb++)
		sum += map[p.x - halfmax.x][p.y];
	if (p.x + halfmax.x < halfmax.y && nb++)
		sum += map[p.x + halfmax.x][p.y];
	map[p.x][p.y] += sum / (double)nb + scale;
}

void	square(double **map, t_point p, t_point halfmax, float scale)
{
	float	sum;
	int		i;
	int		j;
	int		half;

	i = p.x;
	j = p.y;
	half = halfmax.x;
	sum = map[i - half][j - half];
	sum += map[i + half][j - half];
	sum += map[i - half][j + half];
	sum += map[i + half][j + half];
	map[i][j] += sum / 4. + scale;
}

void	square_diamond2(double **map, int max, int size, float scale)
{
	int	x;
	int	half;
	int	y;
	int	col;

	half = size / 2;
	y = 0;
	col = 0;
	while (y < max)
	{
		col++;
		x = (col % (2) * half);
		while (x < max)
		{
			diamond(map, init_point(x, y), init_point(half, max), rand() /
					(double)RAND_MAX * scale * 2 - scale);
			x += half * 2;
		}
		y += half;
	}
}

void	square_diamond(double **map, int max, int size, float scale)
{
	int	x;
	int	y;
	int	half;

	half = size / 2;
	if (!half)
		return ;
	y = half;
	while (y < max && (x = half))
	{
		while (x < max)
		{
			square(map, init_point(x, y), init_point(half, max), rand() /
					(double)RAND_MAX * scale * 2 - scale);
			x += size;
		}
		y += size;
	}
	square_diamond2(map, max, size, scale);
	square_diamond(map, max, size / 2, scale / 2.);
}
