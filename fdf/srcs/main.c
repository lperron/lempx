/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/18 16:49:26 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 20:40:12 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void		*get_love(char *command, t_hook *data)
{
	char	*str;
	int		tmp1;
	int		tmp2;
	void	*img;

	tmp1 = 20;
	tmp2 = 20;
	command[ft_strlen(command) - 3] = '\0';
	if (!(str = ft_strjoin(command, "../redheart20.xpm")))
		return (NULL);
	img = mlx_xpm_file_to_image(data->tt->mlx_ptr,
				str, &tmp1, &tmp2);
	free(str);
	return (img);
}

void		set_cam(t_camera *cam)
{
	double eye[3];
	double center[3];
	double up[3];

	lempx_set_vector3(eye, 0., 0., 20.);
	lempx_set_vector3(center, 0., 0., 0.001);
	lempx_set_vector3(up, 0., 1.0, 0.);
	lempx_look_at(cam, eye, center, up);
	lempx_update_camera(cam);
}

void		launch_hooks(t_lempx_ptr *tt, t_camera *cam, t_model *map, char *av)
{
	t_hook	data;

	data.tt = tt;
	data.color_mode = COL_Z;
	data.cam = cam;
	data.scene_size = 1;
	data.mouse = init_point(1920 / 2, 1080 / 2);
	data.scene = malloc(sizeof(t_model *) * data.scene_size);
	data.key = ft_memalloc(sizeof(char) * 500);
	if (!(data.scene) || !(data.key))
		return ;
	data.scene[0] = map;
	data.cam_mode = LEMPX_FPS;
	data.other = get_love(av, &data);
	set_cam(cam);
	mlx_hook(tt->win_ptr, KEYPRESS, KEYPRESSMASK, handle_key_pressed, &data);
	mlx_hook(tt->win_ptr, KEYRELEASE,
			KEYRELEASEMASK, handle_key_released, &data);
	mlx_loop_hook(tt->mlx_ptr, loop_hook, &data);
	mlx_hook(tt->win_ptr, MOTIONNOTIFY, POINTERMOTIONMASK, mouse_hook, &data);
	mlx_hook(tt->win_ptr, DESTROYNOTIFY,
			STRUCTURENOTIFYMASK, destroy_hook, &data);
	put_info(&data);
	render_scene(&data);
	lempx_loop(tt);
}

t_lempx_ptr	*go_lempx(t_lempx_ptr *tt, t_model *map,
		int *map_size, char *argv)
{
	if (map_size[0] < 1 || map_size[1] < 1)
	{
		ft_putendl("Map error");
		lempx_free_model(map);
		return (NULL);
	}
	set_color_from_z(map);
	lempx_model_rotate(map, 90., 0., -180.);
	lempx_model_translate(map, -map_size[0] / 2., -map_size[1] / 2.,
			-map_size[2] / 2.);
	lempx_model_scale(map, 1., 1., 0.1);
	if (argv[0] != '-')
		tt = lempx_init(1920, 1080, argv);
	else
		tt = lempx_init(1920, 1080, "FDF Auto");
	if (!tt)
		return (NULL);
	mlx_destroy_image(tt->mlx_ptr, tt->image);
	tt->size = init_point(1920, SIZE_DIS);
	tt->image = mlx_new_image(tt->mlx_ptr, tt->size.x, tt->size.y);
	if (!(tt->image))
		return (NULL);
	tt->nb_pixel = tt->size.x * tt->size.y;
	return (tt);
}

int			main(int argc, char **argv)
{
	t_model		*map;
	int			map_size[3];
	t_camera	*cam;
	t_lempx_ptr	*tt;

	tt = NULL;
	map_size[2] = INT_MIN;
	if (argc == 1)
	{
		ft_putendl("usage : ./fdf map\n        ./fdf -a SIZE SMOOTHNESS");
		return (0);
	}
	if (!(cam = lempx_init_camera(50.0, 1920 / 995., 0.1, 10000.0)))
		return (0);
	if (!(map = choose_map(argc, argv, map_size)))
	{
		lempx_free_camera(cam);
		return (0);
	}
	if ((tt = go_lempx(tt, map, map_size, argv[1])))
	{
		lempx_init_img(tt);
		launch_hooks(tt, cam, map, argv[0]);
	}
	return (0);
}
