/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempX.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/19 13:06:41 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:06:16 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LEMPX_H
# define LEMPX_H

# include "libft.h"
# include "mlx.h"
# include "lempxdef.h"
# include <pthread.h>
# include <stdlib.h>

# define NB_THREADS 8

struct							s_point
{
	int							x;
	int							y;
};

typedef	struct s_point			t_point;

struct							s_accu
{
	double							incr;
	double							sum;
};

typedef	struct s_accu			t_accu;

typedef	double					*t_vector;

enum							e_ptype{LEMPX_POINTS, LEMPX_LINES,
	LEMPX_LINE_STRIP, LEMPX_LINE_LOOP,
	LEMPX_TRIANGLES, LEMPX_TRIANGLE_STRIP,
	LEMPX_TRIANGLE_FAN, LEMPX_QUADS,
	LEMPX_QUAD_STRIP, LEMPX_POLYGON, LEMPX_WIREFRAME};
enum							e_proj{LEMPX_PERSPECTIVE, LEMPX_ORTHO};
enum							e_cam_mode{LEMPX_STATIC, LEMPX_FPS,
	LEMPX_ARCBALL};

typedef	enum e_cam_mode			t_cam_mode;

union							u_color
{
	unsigned char				rgb[4];
	unsigned int				color;
};

typedef	union u_color			t_color;

struct							s_camera
{
	double						fovy;
	double						fovx;
	double						ratio;
	double						near;
	double						far;
	double						top;
	double						bottom;
	double						left;
	double						right;
	double						eye[3];
	double						center[3];
	double						up[3];
	double						view[4][4];
	double						projection[4][4];
	double						cam_matrix[4][4];
	enum e_proj					proj_type;
};

struct							s_vertex
{
	t_color						color;
	double						point[4];
	double						clip[4];
	double						ndc[4];
	t_point						screen_pos;
	unsigned char				sc;
};

typedef	struct s_vertex			t_vertex;

struct							s_primitive
{
	enum e_ptype				type;
	t_vertex					*vertex;
	int							nb_vertex;
	int							index;
	t_color						color;
};

typedef	struct s_primitive		t_primitive;

struct							s_lempx
{
	t_point						size;
	int							nb_pixel;
	void						*mlx_ptr;
	void						*win_ptr;
	void						*image;
	double						*depth;
	int							*img_data;
	int							endian;
	int							bits_per_pixel;
	int							size_line;
	pthread_t					thread_prim[NB_THREADS];
};

typedef	struct s_lempx			t_lempx_ptr;

struct							s_thread_data
{
	t_primitive					*prim;
	t_lempx_ptr					*tt;
	enum e_ptype				ty;
	void						*stuff;
};

typedef	struct s_thread_data	t_thread_data;

struct							s_model
{
	double						model_view[4][4];
	enum e_ptype				type;
	t_primitive					*primitive;
	int							nb_primitive;
	t_color						color;
};

typedef	struct s_model			t_model;
typedef	struct s_camera			t_camera;

/*
** lempx_utils.c
*/

void							lempx_set_identity(double m[4][4]);
void							lempx_copy_matrix(double dest[4][4],
								double src[4][4]);
double							get_n_up_coef(t_accu *accu);
double							get_depth(double perc, t_vertex *v1,
								t_vertex *v2);

unsigned int					get_colorx(double p, t_vertex *v1,
								t_vertex *v2);

/*
** t_point.c
*/

t_point							init_point(int x, int y);
/*
**	lempx.c
*/

t_lempx_ptr						*lempx_init(int x, int y, char *name);
void							lempx_destroy(t_lempx_ptr *ptr);
void							lempx_loop(t_lempx_ptr *tt);
int								lempx_init_img(t_lempx_ptr *tt);

/*
** lempx_camera.c
*/

t_camera						*lempx_init_camera(double fov, double ratio,
								double near, double far);
void							lempx_look_at(t_camera *cam, t_vector eye,
								t_vector center, t_vector up);
void							lempx_free_camera(t_camera *cam);
void							lempx_update_camera(t_camera *cam);

/*
**  lempx_projection.c
*/
void							lempx_set_perspective(t_camera *cam);
void							lempx_set_ortho(t_camera *cam);
void							lempx_change_fov(t_camera *cam, double fov);
void							lempx_change_ratio(t_camera *cam, double ratio);
void							lempx_set_projection(t_camera *cam,
								enum e_proj p);

/*
** lempx_rotate.c
*/

void							lempx_rotate(double m[4][4], double a,
								double b, double c);
void							lempx_axis_rotate(double m[4][4],
								double a[3], double t);

/*
** lempx_translate.c
*/

void							lempx_translate(double m[4][4], double x,
								double y, double z);

/*
** lempx_scale.c
*/

void							lempx_scale(double m[4][4], double x,
								double y, double z);

/*
** lempx_viewport.c
*/

t_point							ndc_to_screen(t_point screen_size,
								double x, double y);

/*
** lempx_color.c
*/

unsigned int					lempx_rgb(unsigned int r, unsigned int g,
								unsigned int b);
void							color_interpolation(t_vertex *v1, t_vertex *v2,
								t_vertex *n1, t_vertex *n2);
/*
** lempx_vertex.c
*/

t_vertex						init_vertex_color(double x, double y,
								double z, unsigned int color);
t_vertex						init_vertex(double x, double y, double z);
void							lempx_omega_division(t_vertex *vert);

/*
** lempx_primitive.c
*/
t_primitive						lempx_init_primitive(int size, enum e_ptype t);
void							lempx_primitive_add_vertex(t_primitive *prim,
								double x, double y, double z);
void							lempx_primitive_set_color(t_primitive *prim,
								unsigned int color);
void							lempx_free_primitive(t_primitive *prim);
/*
** lempx_model.c
*/

t_model							lempx_init_model(enum e_ptype pr);
void							lempx_model_new_primitive(t_model *mod,
								int nb_vertex);
void							lempx_model_add_vertex(t_model *mod, double x,
								double y, double z);
void							lempx_model_set_color(t_model *mod,
								unsigned int r, unsigned int g, unsigned int b);
void							lempx_free_model(t_model *mod);

/*
** lempx_model_transform
*/

void							lempx_model_scale(t_model *mod, double x,
								double y, double z);
void							lempx_model_rotate(t_model *mod, double x,
								double y, double z);
void							lempx_model_translate(t_model *mod, double x,
								double y, double z);
void							lempx_model_axis_rotate(t_model *mod,
								double a[3], double t);

/*
** lempx_model_to_screen.c
*/
void							lempx_render_model(t_model *m, t_camera *cam,
								t_lempx_ptr *tt);
/*
** lempx_model_to_clip;
*/
void							lempx_primitive_to_clip(t_primitive *prim,
								double m[4][4]);
int								lempx_model_to_clip(t_model *mod,
								double m[4][4]);

/*
** Bresenham.c
*/

void							lempx_draw_line(t_vertex *vert1,
								t_vertex *v2, t_lempx_ptr *tt);
unsigned int					get_colorx(double perc, t_vertex *v1,
								t_vertex *v2);

/*
** lempx_matrix.c
*/

void							fill_matrix(double m[4][4],
								t_point s, ...);
void							set_matrix_from_matrix(double m1[4][4],
								double m2[4][4], t_point s);
void							print_matrix(double m[4][4], t_point s);
void							lempx_matrix_multiplication(double m1[4][4],
								double m2[4][4]);
void							lempx_fill_matrix4_vector3(double m[4][4],
								t_vector v, int line, double four);

/*
**lempx_vector.c
*/
void							lempx_cross_product3(t_vector dst,
								t_vector v1, t_vector v2);
void							set_vector(t_vector v, int s, ...);
double							scalar_product(t_vector v1, t_vector v2, int s);
void							lambda_vector_multiplication(double l,
								t_vector v, int s);
void							fill_vector(t_vector v, int s, va_list ap);

/*
**lempx_vector2.c
*/
void							print_vector(t_vector v, int s);
double							norm_vector(t_vector v, int s);
void							lempx_vector_normalize3(t_vector v);
void							lempx_vector_normalize_cpy3(t_vector dst,
								t_vector v);
void							lempx_vector_cpy(t_vector dst, t_vector src,
								int s);
/*
** lempx_vector3.c
*/
void							lempx_set_vector3(t_vector dst,
								double x, double y, double z);

/*
** lempx_line_clipping.c
*/

int								lempx_line_clipping(t_vertex *v1,
								t_vertex *v2, t_lempx_ptr *tt);

/*
** lempx_wireframe.c
*/
void							lempx_wireframe(t_primitive *prim,
								t_lempx_ptr *tt);
#endif
