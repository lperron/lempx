/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_point.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/18 15:24:17 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/09 14:47:36 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef T_POINT_H
# define T_POINT_H

struct					s_point
{
	int	x;
	int	y;
};

typedef	struct s_point	t_point;

t_point					init_point(int x, int y);

#endif
