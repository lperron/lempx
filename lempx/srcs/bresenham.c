/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bresenham.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/25 15:39:13 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:12:27 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void		lempx_plot_dot(t_point coord,
				t_lempx_ptr *tt, double depth, int col)
{
	int			pos;

	if (coord.x < 0 || coord.y < 0 || coord.x >= tt->size.x ||
			coord.y >= tt->size.y)
		return ;
	pos = coord.x + coord.y * tt->size.x;
	if (depth > tt->depth[pos])
		return ;
	tt->img_data[pos] = mlx_get_color_value(tt->mlx_ptr, col);
	tt->depth[pos] = depth;
}

void		init_accu(t_accu *accu, t_vertex *v, t_vertex *w)
{
	int	nb_pxl;

	nb_pxl = ft_max(ft_abs(v->screen_pos.x - w->screen_pos.x),
			ft_abs(v->screen_pos.y - w->screen_pos.y));
	accu->incr = 1. / (double)nb_pxl;
	accu->sum = 0;
}

void		bresenham_low(t_vertex *v, t_vertex *w, t_lempx_ptr *tt, t_accu *ac)
{
	t_point	dxy;
	int		yi;
	t_point c;
	int		d;

	dxy = init_point(w->screen_pos.x - v->screen_pos.x, 2 * (w->screen_pos.y -
			v->screen_pos.y));
	init_accu(ac, v, w);
	yi = 1;
	if (dxy.y < 0 && (yi = -1))
		dxy.y *= -1;
	d = dxy.y - dxy.x;
	dxy.x *= 2;
	c = init_point(v->screen_pos.x - 1, v->screen_pos.y);
	while (++c.x < w->screen_pos.x)
	{
		lempx_plot_dot(c, tt, get_depth(ac->sum, v, w),
					get_colorx(get_n_up_coef(ac), v, w));
		if (d > 0)
		{
			c.y += yi;
			d -= dxy.x;
		}
		d += dxy.y;
	}
}

void		bresenham_hi(t_vertex *v, t_vertex *w, t_lempx_ptr *tt, t_accu *ac)
{
	t_point	dxy;
	int		xi;
	t_point c;
	int		d;

	dxy = init_point(2 * (w->screen_pos.x - v->screen_pos.x), w->screen_pos.y -
			v->screen_pos.y);
	init_accu(ac, v, w);
	xi = 1;
	if (dxy.x < 0 && (xi = -1))
		dxy.x *= -1;
	d = dxy.x - dxy.y;
	dxy.y *= 2;
	c = init_point(v->screen_pos.x, v->screen_pos.y - 1);
	while (++c.y < w->screen_pos.y)
	{
		lempx_plot_dot(c, tt, get_depth(ac->sum, v, w),
					get_colorx(get_n_up_coef(ac), v, w));
		if (d > 0)
		{
			c.x += xi;
			d -= dxy.y;
		}
		d += dxy.x;
	}
}

void		lempx_draw_line(t_vertex *v1, t_vertex *v2, t_lempx_ptr *tt)
{
	t_accu	accu;

	if (ft_abs(v2->screen_pos.y - v1->screen_pos.y) <
			ft_abs(v2->screen_pos.x - v1->screen_pos.x))
	{
		if (v1->screen_pos.x > v2->screen_pos.x)
			bresenham_low(v2, v1, tt, &accu);
		else
			bresenham_low(v1, v2, tt, &accu);
	}
	else
	{
		if (v1->screen_pos.y > v2->screen_pos.y)
			bresenham_hi(v2, v1, tt, &accu);
		else
			bresenham_hi(v1, v2, tt, &accu);
	}
}
