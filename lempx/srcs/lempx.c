/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/24 20:35:43 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/06 18:08:50 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

int				lempx_init_img(t_lempx_ptr *tt)
{
	int	i;

	if (!(tt->img_data = (int*)mlx_get_data_addr(tt->image,
					&(tt->bits_per_pixel), &(tt->size_line), &(tt->endian))))
	{
		mlx_destroy_image(tt->mlx_ptr, tt->image);
		mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
		free(tt);
		return (0);
	}
	if (tt->depth)
		free(tt->depth);
	if (!(tt->depth = malloc(sizeof(double) * tt->nb_pixel)))
	{
		mlx_destroy_image(tt->mlx_ptr, tt->image);
		mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
		free(tt);
		return (0);
	}
	i = -1;
	while (++i < tt->nb_pixel)
		tt->depth[i] = HUGE_VAL;
	return (1);
}

t_lempx_ptr		*lempx_init(int x, int y, char *name)
{
	t_lempx_ptr	*tt;

	if (!(tt = malloc(sizeof(t_lempx_ptr))))
		return (NULL);
	tt->mlx_ptr = NULL;
	tt->win_ptr = NULL;
	tt->image = NULL;
	tt->depth = NULL;
	tt->size = init_point(x, y);
	if (!(tt->mlx_ptr = mlx_init()) ||
			!(tt->win_ptr = mlx_new_window(tt->mlx_ptr, x, y, name)) ||
			!(tt->image = mlx_new_image(tt->mlx_ptr, x, y)))
	{
		if (tt->win_ptr)
			mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
		free(tt);
		return (NULL);
	}
	tt->nb_pixel = tt->size.x * tt->size.y;
	if (lempx_init_img(tt) == 0)
		return (NULL);
	return (tt);
}

void			lempx_destroy(t_lempx_ptr *tt)
{
	free(tt->depth);
	mlx_destroy_image(tt->mlx_ptr, tt->image);
	mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
	free(tt);
}

void			lempx_loop(t_lempx_ptr *tt)
{
	mlx_loop(tt->mlx_ptr);
}
