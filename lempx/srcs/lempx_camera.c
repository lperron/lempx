/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_camera.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/20 13:46:39 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:50:09 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void		lempx_look_at(t_camera *cam, t_vector e, t_vector c, t_vector u)
{
	double		tmp1[4][4];
	double		fm[3];
	double		s[3];
	double		um[3];

	if (!cam || !e || !c || !u)
		return ;
	lempx_set_vector3(fm, c[0] - e[0], c[1] - e[1], c[2] - e[2]);
	lempx_vector_normalize3(fm);
	lempx_vector_normalize3(u);
	lempx_cross_product3(s, fm, u);
	lempx_vector_normalize3(s);
	lempx_cross_product3(um, s, fm);
	lempx_set_identity(tmp1);
	lempx_fill_matrix4_vector3(tmp1, s, 0, 0.);
	lempx_fill_matrix4_vector3(tmp1, um, 1, 0.);
	lambda_vector_multiplication(-1, fm, 3);
	lempx_fill_matrix4_vector3(tmp1, fm, 2, 0.);
	lempx_translate(tmp1, -e[0], -e[1], -e[2]);
	lempx_copy_matrix(cam->view, tmp1);
	lempx_vector_cpy(cam->eye, e, 3);
	lempx_vector_cpy(cam->center, c, 3);
	lempx_vector_cpy(cam->up, u, 3);
}

t_camera	*lempx_init_camera(double fovy, double ratio,
		double near, double far)
{
	t_camera *cam;

	if (!(cam = malloc(sizeof(t_camera))))
		return (NULL);
	cam->fovy = fovy * M_PI / 180.;
	cam->fovx = cam->fovy * ratio;
	cam->ratio = ratio;
	cam->near = near;
	cam->far = far;
	cam->top = tan(cam->fovy / 2.) * near;
	cam->bottom = -cam->top;
	cam->right = cam->top * ratio;
	cam->left = -cam->right;
	cam->proj_type = 0;
	lempx_set_vector3(cam->up, 0.0, 1.0, 0.0);
	lempx_set_vector3(cam->center, 0.0, 0.0, -1);
	lempx_set_vector3(cam->eye, 0.0, 0.0, 10);
	lempx_set_identity(cam->view);
	lempx_set_perspective(cam);
	lempx_update_camera(cam);
	return (cam);
}

void		lempx_free_camera(t_camera *cam)
{
	free(cam);
	cam = NULL;
}

void		lempx_update_camera(t_camera *cam)
{
	lempx_copy_matrix(cam->cam_matrix, cam->projection);
	lempx_matrix_multiplication(cam->cam_matrix, cam->view);
}
