/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_color.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/23 13:57:19 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:49:20 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

unsigned int	lempx_rgb(unsigned int r, unsigned int g, unsigned int b)
{
	if ((r > 255) | (g > 255) | (b > 255))
		return (0);
	return ((r << 16) | (g << 8) | b);
}

void			color_interpolation(t_vertex *v1, t_vertex *v2, t_vertex *n1,
		t_vertex *n2)
{
	int		i;
	double	t;
	double	d;

	i = -1;
	t = 1;
	while (++i < 3 && v1->clip[i] == v2->clip[i])
		;
	if (i == 3)
	{
		n1->color.color = v1->color.color;
		n2->color.color = v1->color.color;
		return ;
	}
	d = 1. / (v1->clip[i] - v2->clip[i]);
	t = (n1->clip[i] - v1->clip[i]) * d;
	t *= t < 0 ? -1 : 1;
	n1->color.color = get_colorx(t, v1, v2);
	t = (n2->clip[i] - v1->clip[i]) * d;
	t *= t < 0 ? -1 : 1;
	n2->color.color = get_colorx(t, v1, v2);
}
