/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_line_clipping.c                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/05 13:09:58 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:56:09 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void		sc_bitmapv(t_vertex *v)
{
	double	w;

	w = v->clip[3];
	v->sc = 0;
	if (v->clip[1] > w)
		v->sc += 1;
	if (v->clip[1] < -w)
		v->sc += (1U << 1);
	if (v->clip[0] > w)
		v->sc += (1U << 2);
	if (v->clip[0] < -w)
		v->sc += (1U << 3);
	if (v->clip[2] > w)
		v->sc += (1U << 4);
	if (v->clip[2] < -w)
		v->sc += (1U << 5);
}

t_vertex	clip_axis(t_vertex *v, t_vertex *v2, int axis)
{
	t_vertex	new;
	double		t;
	double		sign;
	double		d[4];

	if (axis == 0)
		sign = v->sc & 4U ? 1. : -1.;
	else if (axis == 1)
		sign = v->sc & 1U ? 1. : -1.;
	else
		sign = v->sc & 16U ? 1. : -1.;
	t = -1 * ((v->clip[3] - sign * v->clip[axis]) / (v->clip[3] - sign *
				v->clip[axis] - (v2->clip[3] - sign * v2->clip[axis])));
	set_vector(d, 4, v->clip[0] - v2->clip[0], v->clip[1] -
		v2->clip[1], v->clip[2] - v2->clip[2], v->clip[3] - v2->clip[3]);
	set_vector(new.clip, 4, v->clip[0] + t * d[0], v->clip[1] + t *
			d[1], v->clip[2] + t * d[2], v->clip[3] + t * d[3]);
	sc_bitmapv(&new);
	return (new);
}

t_vertex	vertex_clipping(t_vertex *v, t_vertex *u)
{
	t_vertex	new;
	int			i;

	if (v->sc == 0)
		return (*v);
	set_vector(new.clip, 4, v->clip[0], v->clip[1], v->clip[2], v->clip[3]);
	new.sc = v->sc;
	i = -1;
	while (new.sc && ++i < 2)
	{
		if (new.sc & 16U || new.sc & 32U)
			new = clip_axis(&new, u, 2);
		if (new.sc & 4U || new.sc & 8U)
			new = clip_axis(&new, u, 0);
		if (new.sc & 1U || new.sc & 2U)
			new = clip_axis(&new, u, 1);
	}
	return (new);
}

int			lempx_draw_finish_line(t_vertex *u, t_vertex *v, t_lempx_ptr *tt)
{
	lempx_omega_division(v);
	lempx_omega_division(u);
	v->screen_pos = ndc_to_screen(tt->size, v->ndc[0], v->ndc[1]);
	u->screen_pos = ndc_to_screen(tt->size, u->ndc[0], u->ndc[1]);
	lempx_draw_line((v), (u), tt);
	return (1);
}

int			lempx_line_clipping(t_vertex *v, t_vertex *u, t_lempx_ptr *tt)
{
	t_vertex	n[2];

	if (v->sc & u->sc)
		return (1);
	if (!(v->sc | u->sc))
		return (lempx_draw_finish_line(u, v, tt));
	n[0] = vertex_clipping(v, u);
	if (n[0].sc)
		return (1);
	n[1] = vertex_clipping(u, v);
	if (n[1].sc)
		return (1);
	color_interpolation(v, u, &n[0], &n[1]);
	return (lempx_draw_finish_line(&n[0], &n[1], tt));
}
