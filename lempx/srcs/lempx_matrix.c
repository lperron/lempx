/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_matrix.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/27 14:25:54 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:52:55 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void		fill_matrix(double m[4][4], t_point s, ...)
{
	int			count;
	int			max;
	va_list		ap;

	va_start(ap, s);
	max = s.x * s.y;
	count = -1;
	while (++count < max)
		m[count / s.x][count % s.x] = va_arg(ap, double);
	va_end(ap);
}

void		set_matrix_from_matrix(double m1[4][4], double m2[4][4], t_point s)
{
	int		j;
	int		i;

	if (!m1 || !m2)
		return ;
	j = -1;
	while (++j < s.y && (i = -1))
		while (++i < s.x)
			m1[j][i] = m2[j][i];
}

void		print_matrix(double m[4][4], t_point s)
{
	int	i;
	int	j;

	i = -1;
	while (++i < s.y)
	{
		j = -1;
		ft_putchar('(');
		while (++j < s.x)
		{
			if (j != s.x - 1)
				ft_printf("%6.3f, ", m[i][j]);
			else
				ft_printf("%6.3f", m[i][j]);
		}
		ft_putstr(")\n");
	}
}

void		lempx_matrix_multiplication(double m1[4][4], double m2[4][4])
{
	double		m3[4][4];
	int			i;
	int			j;
	int			k;

	j = -1;
	set_matrix_from_matrix(m3, m1, init_point(4, 4));
	while (++j < 4 && (i = -1))
		while (++i < 4 && (k = -1)
				&& !(m1[j][i] = 0))
			while (++k < 4)
				m1[j][i] += m3[j][k] * m2[k][i];
}

void		lempx_fill_matrix4_vector3(double m[4][4],
		t_vector v, int i, double four)
{
	m[i][0] = v[0];
	m[i][1] = v[1];
	m[i][2] = v[2];
	m[i][3] = four;
}
