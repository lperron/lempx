/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_model.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/23 16:53:44 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/09 14:42:27 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

t_model	lempx_init_model(enum e_ptype t)
{
	t_model	mod;

	mod.nb_primitive = 0;
	mod.type = t;
	lempx_set_identity(mod.model_view);
	mod.primitive = NULL;
	mod.color.color = lempx_rgb(255, 255, 255);
	return (mod);
}

void	lempx_model_new_primitive(t_model *mod, int nb_vertex)
{
	int			i;
	t_primitive	*tmp;

	if (!(tmp = malloc(sizeof(t_primitive) * (mod->nb_primitive + 1))))
		return ;
	i = -1;
	while (++i < mod->nb_primitive)
		tmp[i] = mod->primitive[i];
	mod->nb_primitive++;
	tmp[mod->nb_primitive - 1] = lempx_init_primitive(nb_vertex, mod->type);
	free(mod->primitive);
	mod->primitive = tmp;
}

void	lempx_model_add_vertex(t_model *mod, double x, double y, double z)
{
	lempx_primitive_add_vertex(&(mod->primitive[mod->nb_primitive - 1]),
			x, y, z);
}

void	lempx_model_set_color(t_model *mod, unsigned int r, unsigned int g,
		unsigned int b)
{
	lempx_primitive_set_color(&(mod->primitive[mod->nb_primitive - 1]),
			lempx_rgb(r, g, b));
}

void	lempx_free_model(t_model *mod)
{
	int	i;

	i = -1;
	while (++i < mod->nb_primitive)
		lempx_free_primitive(&(mod->primitive[i]));
	free(mod->primitive);
}
