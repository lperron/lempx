/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_model_to_clip.c                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/07 18:36:43 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:53:38 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void		lempx_primitive_to_clip(t_primitive *prim, double m[4][4])
{
	int			i;
	t_vertex	*vertex;

	i = -1;
	while (++i < prim->nb_vertex)
	{
		vertex = &(prim->vertex[i]);
		vertex->clip[0] = m[0][0] * vertex->point[0] + m[0][1] *
	vertex->point[1] + m[0][2] * vertex->point[2] + m[0][3] * vertex->point[3];
		vertex->clip[1] = m[1][0] * vertex->point[0] + m[1][1] *
	vertex->point[1] + m[1][2] * vertex->point[2] + m[1][3] * vertex->point[3];
		vertex->clip[2] = m[2][0] * vertex->point[0] + m[2][1] *
	vertex->point[1] + m[2][2] * vertex->point[2] + m[2][3] * vertex->point[3];
		vertex->clip[3] = m[3][0] * vertex->point[0] + m[3][1] *
	vertex->point[1] + m[3][2] * vertex->point[2] + m[3][3] * vertex->point[3];
	}
}

int			lempx_model_to_clip(t_model *mod, double m[4][4])
{
	double		tmp[4][4];
	t_point		s;
	int			i;

	if (!m)
		return (0);
	s = init_point(4, 4);
	set_matrix_from_matrix(tmp, m, s);
	lempx_matrix_multiplication(tmp, mod->model_view);
	i = -1;
	while (++i < mod->nb_primitive)
		lempx_primitive_to_clip(&(mod->primitive[i]), tmp);
	return (1);
}
