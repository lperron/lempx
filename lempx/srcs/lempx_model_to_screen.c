/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_model_to_screen.c                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/24 15:19:39 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:00:12 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void		sc_bitmap(t_model *mod)
{
	int		i;
	int		j;
	double	w;

	i = -1;
	while (++i < mod->nb_primitive && (j = -1))
	{
		while (++j < mod->primitive[i].nb_vertex)
		{
			mod->primitive[i].vertex[j].sc = 0;
			w = mod->primitive[i].vertex[j].clip[3];
			if (mod->primitive[i].vertex[j].clip[1] > w)
				mod->primitive[i].vertex[j].sc += 1;
			if (mod->primitive[i].vertex[j].clip[1] < -w)
				mod->primitive[i].vertex[j].sc += (1U << 1);
			if (mod->primitive[i].vertex[j].clip[0] > w)
				mod->primitive[i].vertex[j].sc += (1U << 2);
			if (mod->primitive[i].vertex[j].clip[0] < -w)
				mod->primitive[i].vertex[j].sc += (1U << 3);
			if (mod->primitive[i].vertex[j].clip[2] > w)
				mod->primitive[i].vertex[j].sc += (1U << 4);
			if (mod->primitive[i].vertex[j].clip[2] < -w)
				mod->primitive[i].vertex[j].sc += (1U << 5);
		}
	}
}

void		lempx_render_primitive(t_primitive *prim, t_lempx_ptr *tt,
		enum e_ptype ty)
{
	int	i;

	i = -1;
	if (ty == LEMPX_LINE_LOOP)
		while (++i < prim->nb_vertex)
			lempx_line_clipping(&(prim->vertex[i]), &(prim->vertex[(i + 1)
							% prim->nb_vertex]), tt);
	else if (ty == LEMPX_LINE_STRIP)
		while (++i < prim->nb_vertex - 1)
			lempx_line_clipping(&(prim->vertex[i]),
						&(prim->vertex[i + 1]), tt);
	else if (ty == LEMPX_LINES)
		while (++i < prim->nb_vertex - 1)
		{
			lempx_line_clipping(&(prim->vertex[i]),
						&(prim->vertex[i + 1]), tt);
			i++;
		}
	else if (ty == LEMPX_WIREFRAME)
		lempx_wireframe(prim, tt);
}

static int	prim_func(void *data)
{
	t_thread_data *d;

	d = (t_thread_data *)data;
	lempx_render_primitive(d->prim, d->tt, d->ty);
	return (0);
}

void		lempx_render_model(t_model *mod, t_camera *cam, t_lempx_ptr *tt)
{
	int				i;
	int				j;
	t_thread_data	data[NB_THREADS];

	if (!mod || !cam || !tt)
		return ;
	lempx_model_to_clip(mod, cam->cam_matrix);
	sc_bitmap(mod);
	i = 0;
	while (i < mod->nb_primitive && (j = -1))
	{
		while (++j < NB_THREADS && i + j < mod->nb_primitive)
		{
			data[j].prim = &(mod->primitive[i + j]);
			data[j].tt = tt;
			data[j].ty = mod->type;
			pthread_create(&(tt->thread_prim[j]),
					NULL, (void*)prim_func, &(data[j]));
		}
		j = -1;
		while (++j < NB_THREADS && i + j < mod->nb_primitive)
			pthread_join((tt->thread_prim[j]), NULL);
		i += NB_THREADS;
	}
}
