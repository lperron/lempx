/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_model_transform.c                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/24 15:05:25 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/14 11:24:33 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void		lempx_model_scale(t_model *mod, double x, double y, double z)
{
	lempx_scale(mod->model_view, x, y, z);
}

void		lempx_model_rotate(t_model *mod, double x, double y, double z)
{
	lempx_rotate(mod->model_view, x, y, z);
}

void		lempx_model_axis_rotate(t_model *mod, double a[3], double t)
{
	lempx_axis_rotate(mod->model_view, a, t);
}

void		lempx_model_translate(t_model *mod, double x, double y, double z)
{
	lempx_translate(mod->model_view, x, y, z);
}
