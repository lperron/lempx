/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_primitive.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/23 15:39:40 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/09 14:42:57 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

t_primitive	lempx_init_primitive(int size, enum e_ptype t)
{
	t_primitive	prim;

	prim.nb_vertex = 0;
	if ((prim.vertex = malloc(sizeof(t_vertex) * size)))
		prim.nb_vertex = size;
	prim.type = t;
	prim.index = 0;
	prim.color.color = lempx_rgb(255, 255, 255);
	return (prim);
}

void		lempx_primitive_add_vertex(t_primitive *prim, double x,
		double y, double z)
{
	if (prim->index >= prim->nb_vertex)
		return ;
	prim->vertex[prim->index] = init_vertex_color(x, y, z, prim->color.color);
	(prim->index)++;
}

void		lempx_primitive_set_color(t_primitive *prim, unsigned int color)
{
	prim->color.color = color;
}

void		lempx_free_primitive(t_primitive *prim)
{
	free(prim->vertex);
}
