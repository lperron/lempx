/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_projection.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/07 18:55:13 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:59:12 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void		lempx_set_perspective(t_camera *cam)
{
	cam->projection[0][0] = -cam->near / cam->right;
	cam->projection[0][1] = 0.;
	cam->projection[0][2] = 0.;
	cam->projection[0][3] = 0.;
	cam->projection[1][0] = 0.;
	cam->projection[1][1] = cam->near / cam->top;
	cam->projection[1][2] = 0.;
	cam->projection[1][3] = 0.;
	cam->projection[2][0] = 0.;
	cam->projection[2][1] = 0.;
	cam->projection[2][2] = ((cam->far + cam->near) / (cam->near - cam->far));
	cam->projection[2][3] = (2. * cam->far * cam->near) /
		(cam->near - cam->far);
	cam->projection[3][0] = 0.;
	cam->projection[3][1] = 0.;
	cam->projection[3][2] = -1.;
	cam->projection[3][3] = 0.;
}

void		lempx_set_ortho(t_camera *cam)
{
	cam->projection[0][0] = -(2.) / (cam->right - cam->left);
	cam->projection[0][1] = 0.;
	cam->projection[0][2] = 0.;
	cam->projection[0][3] = -(cam->left + cam->right) /
		(cam->right - cam->left);
	cam->projection[1][0] = 0.;
	cam->projection[1][1] = 2. / (cam->top - cam->bottom);
	cam->projection[1][2] = 0.;
	cam->projection[1][3] = (cam->top + cam->bottom) /
		(cam->top - cam->bottom);
	cam->projection[2][0] = 0.;
	cam->projection[2][1] = 0.;
	cam->projection[2][2] = -(2 / (cam->far - cam->near));
	cam->projection[2][3] = -(cam->far + cam->near) / (cam->far - cam->near);
	cam->projection[3][0] = 0.;
	cam->projection[3][1] = 0.;
	cam->projection[3][2] = 0.;
	cam->projection[3][3] = 1.;
}

void		lempx_set_projection(t_camera *cam, enum e_proj p)
{
	cam->proj_type = p;
	if (p == 0 || cam->near == cam->far || cam->top == cam->bottom ||
			cam->left == cam->right)
		lempx_set_perspective(cam);
	else
		lempx_set_ortho(cam);
}

void		lempx_change_fov(t_camera *cam, double fovy)
{
	cam->fovy = fovy * M_PI / 180.;
	cam->fovx = cam->fovy * cam->ratio;
	cam->top = tan(cam->fovy / 2.) * cam->near;
	cam->bottom = -cam->top;
	cam->right = cam->top * cam->ratio;
	cam->left = -cam->right;
	lempx_set_projection(cam, cam->proj_type);
}

void		lempx_change_ratio(t_camera *cam, double ratio)
{
	cam->fovx = cam->fovy * ratio;
	cam->ratio = ratio;
	cam->right = cam->top * ratio;
	cam->left = -cam->right;
	lempx_set_projection(cam, cam->proj_type);
}
