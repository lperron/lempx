/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_rotate.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/19 14:17:51 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:58:06 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void	lempx_rotatex(double m[4][4], double co, double si)
{
	double		rot[4][4];

	lempx_set_identity(rot);
	rot[1][1] = co;
	rot[2][1] = -si;
	rot[1][2] = si;
	rot[2][2] = co;
	lempx_matrix_multiplication(m, rot);
}

void	lempx_rotatey(double m[4][4], double co, double si)
{
	double		rot[4][4];

	lempx_set_identity(rot);
	rot[0][0] = co;
	rot[2][0] = si;
	rot[0][2] = -si;
	rot[2][2] = co;
	lempx_matrix_multiplication(m, rot);
}

void	lempx_rotatez(double m[4][4], double co, double si)
{
	double		rot[4][4];

	lempx_set_identity(rot);
	rot[0][0] = co;
	rot[1][0] = -si;
	rot[0][1] = si;
	rot[1][1] = co;
	lempx_matrix_multiplication(m, rot);
}

void	lempx_rotate(double m[4][4], double x, double y, double z)
{
	double	pi180;

	pi180 = M_PI / 180.;
	x = x * pi180;
	y = y * pi180;
	z = z * pi180;
	if (x != 0.)
		lempx_rotatex(m, cos(x), sin(x));
	if (y != 0.)
		lempx_rotatey(m, cos(y), sin(y));
	if (z != 0.)
		lempx_rotatez(m, cos(z), sin(z));
}

void	lempx_axis_rotate(double m[4][4], double a[3], double t)
{
	double	rot[4][4];
	double	c;
	double	s;
	double	mc;

	lempx_set_identity(rot);
	lempx_vector_normalize3(a);
	c = cos(t * M_PI / 180.);
	s = sin(t * M_PI / 180.);
	mc = 1 - c;
	lempx_set_vector3(rot[0], a[0] * a[0] * mc + c, a[0] * a[1] * mc
	- a[2] * s, a[2] * a[0] * mc + a[1] * s);
	lempx_set_vector3(rot[1], a[0] * a[1] * mc + a[2] * s, a[1] * a[1]
	* mc + c, a[1] * a[2] * mc - a[0] * s);
	lempx_set_vector3(rot[2], a[0] * a[2] * mc - a[1] * s, a[1] * a[2]
	* mc + a[0] * s, a[2] * a[2] * mc + c);
	lempx_matrix_multiplication(m, rot);
}
