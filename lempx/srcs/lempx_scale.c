/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_scale.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/19 14:52:33 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/09 14:43:13 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void	lempx_scale(double m[4][4], double x, double y, double z)
{
	double		trans[4][4];

	lempx_set_identity(trans);
	trans[0][0] = x;
	trans[1][1] = y;
	trans[2][2] = z;
	lempx_matrix_multiplication(m, trans);
}
