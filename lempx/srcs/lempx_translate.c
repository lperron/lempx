/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_translate.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/19 14:45:31 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/13 18:24:16 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void	lempx_translate(double m[4][4], double x, double y, double z)
{
	double		trans[4][4];

	lempx_set_identity(trans);
	trans[0][3] = x;
	trans[1][3] = y;
	trans[2][3] = z;
	lempx_matrix_multiplication(m, trans);
}
