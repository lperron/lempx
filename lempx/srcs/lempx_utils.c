/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_utils.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/19 14:09:53 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 12:57:20 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

void			lempx_set_identity(double m[4][4])
{
	int	i;
	int	j;

	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			m[i][j] = i == j ? 1. : 0.;
}

void			lempx_copy_matrix(double dest[4][4], double src[4][4])
{
	int	i;
	int	j;

	if (!dest || !src)
		return ;
	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			dest[i][j] = src[i][j];
}

double			get_n_up_coef(t_accu *accu)
{
	double ret;

	ret = accu->sum;
	accu->sum += accu->incr;
	return (ret);
}

double			get_depth(double perc, t_vertex *v1, t_vertex *v2)
{
	return ((1 - perc) * v1->ndc[2] + perc * v2->ndc[2]);
}

unsigned int	get_colorx(double p, t_vertex *v1, t_vertex *v2)
{
	t_color				new_col;

	if (v1->color.color == v2->color.color)
		return (v2->color.color);
	new_col.rgb[3] = 0;
	new_col.rgb[2] = (char)((1 - p) * v1->color.rgb[2] + p * v2->color.rgb[2]);
	new_col.rgb[1] = (char)((1 - p) * v1->color.rgb[1] + p * v2->color.rgb[1]);
	new_col.rgb[0] = (char)((1 - p) * v1->color.rgb[0] + p * v2->color.rgb[0]);
	return (new_col.color);
}
