/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_vector.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/02 21:35:05 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/11 12:25:26 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <math.h>
#include <stdarg.h>
#include "lempx.h"

void		fill_vector(t_vector v, int s, va_list ap)
{
	int	count;

	count = -1;
	while (++count < s)
		v[count] = va_arg(ap, double);
}

void		lempx_cross_product3(t_vector dst, t_vector v1, t_vector v2)
{
	set_vector(dst, 3,
	v1[1] * v2[2] - v1[2] * v2[1],
	v1[2] * v2[0] - v1[0] * v2[2],
	v1[0] * v2[1] - v1[1] * v2[0]);
}

void		set_vector(t_vector v, int s, ...)
{
	va_list		ap;

	va_start(ap, s);
	fill_vector(v, s, ap);
	va_end(ap);
}

double		scalar_product(t_vector v1, t_vector v2, int s)
{
	int		i;
	double	res;

	i = -1;
	res = 0;
	while (++i < s)
		res += v1[i] * v2[i];
	return (res);
}

void		lambda_vector_multiplication(double l, t_vector v, int s)
{
	int	i;

	i = -1;
	if (!v)
		return ;
	while (++i < s)
		v[i] *= l;
}
