/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_vector2.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/02 22:05:09 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:00:53 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"
#include <math.h>

void		print_vector(t_vector v, int s)
{
	int	i;

	i = -1;
	if (!s)
		return ;
	ft_putchar('(');
	while (++i < s)
	{
		if (i != s - 1)
			ft_printf("%6.3f, ", v[i]);
		else
			ft_printf("%6.3f", v[i]);
	}
	ft_putstr(")\n");
}

double		norm_vector(t_vector v, int s)
{
	return (sqrt(scalar_product(v, v, s)));
}

void		lempx_vector_normalize3(t_vector v)
{
	double tmp;

	if (v[0] == 0. && v[1] == 0. && v[2] == 0.)
		return ;
	tmp = 1 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] *= tmp;
	v[1] *= tmp;
	v[2] *= tmp;
}

void		lempx_vector_normalize_cpy3(t_vector dst, t_vector v)
{
	double tmp;

	if (v[0] == 0 && v[1] == 0 && v[2] == 0)
		tmp = 1;
	else
		tmp = 1 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	dst[0] = v[0] * tmp;
	dst[1] = v[1] * tmp;
	dst[2] = v[2] * tmp;
}

void		lempx_vector_cpy(t_vector dst, t_vector src, int s)
{
	int i;

	i = -1;
	while (++i < s)
		dst[i] = src[i];
}
