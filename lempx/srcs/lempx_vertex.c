/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_vertex.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/23 14:09:49 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:01:37 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

t_vertex	init_vertex_color(double x, double y, double z, unsigned int color)
{
	t_vertex	vert;

	vert.point[0] = x;
	vert.point[1] = y;
	vert.point[2] = z;
	vert.point[3] = 1.;
	vert.clip[0] = 0.;
	vert.clip[1] = 0.;
	vert.clip[2] = 0.;
	vert.clip[3] = 0.;
	vert.ndc[0] = 0.;
	vert.ndc[1] = 0.;
	vert.ndc[2] = 0.;
	vert.ndc[3] = 0.;
	vert.screen_pos.x = 0;
	vert.screen_pos.y = 0;
	vert.color.color = color;
	return (vert);
}

t_vertex	init_vertex(double x, double y, double z)
{
	t_vertex	vert;

	vert.point[0] = x;
	vert.point[1] = y;
	vert.point[2] = z;
	vert.point[3] = 1.;
	vert.clip[0] = 0.;
	vert.clip[1] = 0.;
	vert.clip[2] = 0.;
	vert.clip[3] = 0.;
	vert.ndc[0] = 0.;
	vert.ndc[1] = 0.;
	vert.ndc[2] = 0.;
	vert.ndc[3] = 0.;
	vert.screen_pos.x = 0;
	vert.screen_pos.y = 0;
	vert.color.color = lempx_rgb(255, 255, 255);
	return (vert);
}

void		lempx_omega_division(t_vertex *vert)
{
	double tmp;

	tmp = 1. / vert->clip[3];
	if (tmp < 0)
		tmp *= -1;
	vert->ndc[2] = vert->clip[2] * tmp;
	vert->ndc[0] = vert->clip[0] * tmp;
	vert->ndc[1] = vert->clip[1] * tmp;
}
