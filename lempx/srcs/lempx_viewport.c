/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_viewport.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/22 15:39:20 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/09 14:44:11 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

t_point	ndc_to_screen(t_point screen_size, double x, double y)
{
	return (init_point((x / 2. + 0.5) * screen_size.x,
	screen_size.y - (y / 2. + 0.5) * screen_size.y));
}
