/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lempx_wireframe.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/10 13:00:42 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 13:03:32 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lempx.h"

static int	count_line_size(t_primitive *prim)
{
	int		i;
	double	x;

	i = 0;
	x = prim->vertex[0].point[0];
	while (++i < prim->nb_vertex)
		if (prim->vertex[i].point[0] == x)
			return (i);
	return (-1);
}

static int	draw_wline(void *data)
{
	int				i;
	t_thread_data	*d;
	int				*stuff;

	d = (t_thread_data *)data;
	stuff = (int *)d->stuff;
	stuff[1] *= stuff[0];
	i = -1;
	while (++i < stuff[0] - 1 && i + stuff[1] < d->prim->nb_vertex - 1)
		lempx_line_clipping(&(d->prim->vertex[i + stuff[1]]),
			&(d->prim->vertex[stuff[1] + i + 1]), d->tt);
	return (0);
}

static int	draw_wcol(void *data)
{
	int				i;
	t_thread_data	*d;
	int				*stuff;

	d = (t_thread_data *)data;
	stuff = (int *)d->stuff;
	i = stuff[1];
	while (stuff[0] + i < d->prim->nb_vertex)
	{
		lempx_line_clipping(&(d->prim->vertex[i]),
				&(d->prim->vertex[i + stuff[0]]), d->tt);
		i += stuff[0];
	}
	return (0);
}

static void	lempx_wireframe2(t_primitive *prim, t_lempx_ptr *tt, int lsize)
{
	t_thread_data	data[NB_THREADS];
	int				stuff[NB_THREADS][2];
	pthread_t		thread[NB_THREADS];
	int				i;
	int				j;

	stuff[0][0] = lsize;
	i = 0;
	while (i < prim->nb_vertex / stuff[0][0] + 1 && (j = -1))
	{
		while (++j < NB_THREADS && (i + j) < prim->nb_vertex / stuff[0][0] + 1)
		{
			data[j].prim = prim;
			data[j].tt = tt;
			stuff[j][0] = stuff[0][0];
			stuff[j][1] = i + j;
			data[j].stuff = &(stuff[j]);
			pthread_create(&(thread[j]), NULL, (void*)draw_wline, &(data[j]));
		}
		j = -1;
		while (++j < NB_THREADS && (i + j) < prim->nb_vertex / stuff[0][0] + 1)
			pthread_join(thread[j], NULL);
		i += NB_THREADS;
	}
}

void		lempx_wireframe(t_primitive *prim, t_lempx_ptr *tt)
{
	t_thread_data	data[NB_THREADS];
	int				stuff[NB_THREADS][2];
	pthread_t		thread[NB_THREADS];
	int				i;
	int				j;

	stuff[0][0] = count_line_size(prim);
	i = 0;
	while (i < stuff[0][0] && (j = -1))
	{
		while (++j < NB_THREADS && (i + j) < stuff[0][0])
		{
			data[j].prim = prim;
			data[j].tt = tt;
			stuff[j][0] = stuff[0][0];
			stuff[j][1] = i + j;
			data[j].stuff = &(stuff[j]);
			pthread_create(&(thread[j]), NULL, (void*)draw_wcol, &(data[j]));
		}
		j = -1;
		while (++j < NB_THREADS && (i + j) < stuff[0][0])
			pthread_join(thread[j], NULL);
		i += NB_THREADS;
	}
	lempx_wireframe2(prim, tt, stuff[0][0]);
}
